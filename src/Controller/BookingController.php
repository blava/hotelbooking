<?php

namespace App\Controller;

use DateTime;
use PHPUnit\Util\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class BookingController extends AbstractController
{
    const NO_ROOMS_FOUND =  'No Rooms Found';

    public function index(Request $request): Response
    {
        $rooms = $request->query->get('rooms');
        if (empty($rooms)) {
             throw new Exception(BookingController::NO_ROOMS_FOUND);
        }
        return $this->render('booking/index.html.twig', [
            'controller_name' => 'BookingController',
            'rooms' => $rooms,
            'booking_parameters' => [
                                      'start_date' =>  $rooms['booking']['start_booking'] ,
                                      'end_date' => $rooms['booking']['end_booking'],
                                      'guests' => $rooms['booking']['guests']
                        ],
        ]);
    }
}
