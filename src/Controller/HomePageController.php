<?php

namespace App\Controller;

use App\Entity\Booking;
use App\Entity\City;
use App\Entity\Hotel;
use App\Entity\UserBookingInput;
use App\Form\UserBookingInputType;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class HomePageController extends AbstractController
{
    public function index(Request $request, ManagerRegistry $doctrine): Response
    {

        if ($request->getMethod() === 'POST') {

            $requestAr = $request->request->get('user_booking_input');
            $id = $doctrine->getRepository(City::class)->getIdByDestination($requestAr['destination']);
            $rooms = $this->findBookings($id, $doctrine);
            $rooms['booking']['start_booking'] = $requestAr['start_booking'];
            $rooms['booking']['end_booking'] = $requestAr['end_booking'];
            $rooms['booking']['guests'] = $requestAr['guests'];
            return $this->redirectToRoute("booking_homepage", ['rooms' => $rooms]);
        }
            $ubi = new UserBookingInput();
            $ubi->setDestination('');
            $ubi->setStartBooking( new \DateTime());
            $ubi->setEndBooking(new \DateTime());
            $myForm = $this->createForm(UserBookingInputType::class, $ubi);
            return $this->renderForm('home_page/index.html.twig', [
            'form' => $myForm,
        ]);
    }

    public function findBookings(int $city_id,ManagerRegistry $doctrine) : array {
        $city = $doctrine->getRepository(City::class)->find($city_id)->getCity();
        $hotels = $doctrine->getRepository(City::class)->findHotels($city_id);

        $reservation= [];
        $reservation['city'] = $city;
        foreach ($hotels as $index=>$hotel) {
            $reservation['data'][$index]['hotel']=$hotel;
            $room = $doctrine->getRepository(Hotel::class)->findRoom($hotel['id']);
            $reservation['data'][$index]['room'] = $room;
        }
        return ($reservation);
    }

}
