<?php

namespace App\Entity;

use App\Repository\BookingRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BookingRepository::class)
 * @ORM\Table("bookings")
 */
class Booking
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $start_booking;

    /**
     * @ORM\Column(type="date")
     */
    private $end_booking;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $guests;

    /**
     * @ORM\ManyToOne(targetEntity=Room::class, inversedBy="bookings")
     */
    private $room;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStartBooking(): ?\DateTimeInterface
    {
        return $this->start_booking;
    }

    public function setStartBooking(\DateTimeInterface $start_booking): self
    {
        $this->start_booking = $start_booking;

        return $this;
    }

    public function getEndBooking(): ?\DateTimeInterface
    {
        return $this->end_booking;
    }

    public function setEndBooking(\DateTimeInterface $end_booking): self
    {
        $this->end_booking = $end_booking;

        return $this;
    }

    public function getGuests(): ?int
    {
        return $this->guests;
    }

    public function setGuests(?int $guests): self
    {
        $this->guests = $guests;

        return $this;
    }

    public function getRoom(): ?Room
    {
        return $this->room;
    }

    public function setRoom(?Room $room): self
    {
        $this->room = $room;

        return $this;
    }
}
