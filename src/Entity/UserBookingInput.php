<?php

namespace App\Entity;

use App\Repository\UserBookingInputRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UserBookingInputRepository::class)
 */
class UserBookingInput
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $destination;

    /**
     * @ORM\Column(type="integer")
     */
    private $guests;

    /**
     * @ORM\Column(type="date")
     */
    private $start_booking;

    /**
     * @ORM\Column(type="date")
     */
    private $end_booking;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $user_id;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDestination(): ?string
    {
        return $this->destination;
    }

    public function setDestination(?string $destination): self
    {
        $this->destination = $destination;

        return $this;
    }

    public function getGuests(): ?int
    {
        return $this->guests;
    }

    public function setGuests(int $guests): self
    {
        $this->guests = $guests;

        return $this;
    }

    public function getStartBooking(): ?DateTime
    {
        return $this->start_booking;
    }

    public function setStartBooking(DateTime $start_booking): self
    {
        $this->start_booking = $start_booking;

        return $this;
    }

    public function getEndBooking(): ?DateTime
    {
        return $this->end_booking;
    }

    public function setEndBooking(DateTime $end_booking): self
    {
        $this->end_booking = $end_booking;

        return $this;
    }

    public function getUserId(): ?int
    {
        return $this->user_id;
    }

    public function setUserId(?int $user_id): self
    {
        $this->user_id = $user_id;

        return $this;
    }
}
