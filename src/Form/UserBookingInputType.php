<?php

namespace App\Form;

use App\Entity\UserBookingInput;
use Container3eFVxSW\getTexter_TransportsService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserBookingInputType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('destination', TextType::class, ['attr' =>
            ['class' => 'form-control'],
            'label' => 'Choose City'])
            ->add('start_booking', DateType::class, [
                'attr' => [
                    'class' => 'form-control js-datepicker'
                ],
                'label' => 'Start Date',
                'widget' => 'single_text'
            ])
            ->add('end_booking', DateType::class, [
                'attr' => [
                    'class' => 'form-control js-datepicker'
                ],
                'label' => 'End Date',
                'widget' => 'single_text'
            ])
            ->add('guests', ChoiceType::class, [
                'attr' =>
                    [
                        'class' => 'form-control'
                    ],
                'choices' => [
                    '1' => 1,
                    '2' => 2,
                    '3' => 3,
                    '4' => 4
                ],
            ])
            ->add('submit', SubmitType::class, ['label' => 'Book Now!',
                'attr' => [
                    'class' => 'btn btn-lg btn-success']
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UserBookingInput::class
        ]);
    }
}